#INCLUDE=-I$(SRCDIR)/include

#### ALL ####

include omp/Makefile
include cuda/Makefile

all: $(EXECFILEOMP) $(EXECFILECUDA)


clean:
	#### OMP ####
	rm -f $(EXECFILEOMP)
	rm -f $(OBJDIROMP)/*.o
	rm -f $(SRCDIROMP)/*~
	#### CUDA ####
	rm -f $(EXECFILECUDA)
	rm -f $(OBJDIRCUDA)/*.o
	rm -f $(SRCDIRCUDA)/*~
	rm -f *~
