
#ifndef _COMMON_H
#define _COMMON_H

#include <stdint.h>
#include <stdio.h>

#define RANDMAX ((2**31)-1)
#define NBIN    (20)
#define DISTANCE_OUT_FILE "dist_file.dat"
#define MY_DISTANCE_OUT_FILE "my_dist_file.dat"

/******************* Macros for timing **********************/
/*  
    You should define the following vars
    struct timeval begin, end;
    double elapsed;
    int timer = 1;
*/

#define START_TIMER(begin)  if (timer) gettimeofday(&begin, NULL);

#define END_TIMER(end)      if (timer) gettimeofday(&end,   NULL);

//get the total number of s that the code took:
#define ELAPSED_TIME(elapsed, begin, end)            \
    if (timer) elapsed = (end.tv_sec - begin.tv_sec) \
    + ((end.tv_usec - begin.tv_usec)/1000000.0);     \

/************************************************************/

typedef uint32_t UL;

typedef struct {
    UL *rows;        // adjacency lists
    UL *offsets;     // starting points of adjacency lists
    UL *deg;         // array of degrees
    UL nv;           // number of vertices in the graph (offsets has nv+1 elements)
    UL ne;           // number of edges in the graph (elements in rows)
} csrdata;

struct kernel_data {
    char* has_work;
    char* q1;
    char* q2;
    UL* dist;
    UL nv;
    UL d;
}; 

/************************************************************/


/************************************    FUNCTIONS    **********************************************/
// Perform a bfs on a single CPU, return the array of distances from the source s
UL *do_bfs_serial(UL s, csrdata *csrg);
// Build the data struct and do bfs by calling do_bfs_serial
UL *traverse(UL *edges, UL nedges, UL nvertices, UL root, int randsource, int seed);
// Validate the given distances array by calling traverse and comparing the distances
int validate_bfs(UL *edges, UL nedges, UL nvertices, UL root, UL *distances);

// Wrong versions to check the validate function
UL *do_bfs_wrong(UL source, csrdata *csrg, csrdata* csrg_dev, struct kernel_data* dev);
UL *traverse_wrong(UL *edges, UL nedges, UL nvertices, UL root, int randsource, int seed);

// Generate an edge list by extracting random numbers
UL *gen_graph(UL n, UL nedges, unsigned seed);
// Generate RMAT edge list
UL *gen_rmat(UL ned, int scale, float a, float ab, float abc, int seed);
// Read a graph from file
int read_graph_ff(char *fname, UL **edges, UL *nedges, UL *nvertices);

// Do some statistics in the edge list
int compute_dd(UL *edgelist, UL nedges, UL nv);
// Build the CSR data structure from the edge list
int build_csr(UL *edges, UL nedges, UL nv, csrdata *csrg);
// Make the graph undirected by mirroring edges
UL *mirror(UL *ed, UL *ned);
// remove self-loop and multiple edges
UL norm_graph(UL *ed, UL ned);
// Extract a vertex with degree > 0
UL random_source(csrdata *csrg, unsigned seed);

// Useful functions
inline void *Malloc(size_t sz);
inline void *Realloc(void *ptr, size_t sz);
inline FILE *Fopen(const char *path, const char *mode);
inline int cmpedge(const void *p1, const void *p2);
int print_csr(csrdata *in);
int print_edges(UL *ed, UL ned);
int Usage (char *str);
/****************************************************************************************************/

#endif
