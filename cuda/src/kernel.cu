
#include "common.h"

#include <cstdio>

#define WARP_SIZE 32

/* CUDA helpers function */

static inline void cuda_malloc_checked(void** ptr, size_t size)
{
    cudaError_t errorCode = cudaMalloc(ptr, size);
    
    if (errorCode != cudaSuccess) {
        fprintf(stderr, "cuda_malloc_checked: %s\n", cudaGetErrorString(errorCode));
        exit(-1);
    }
}

static inline void cuda_free_checked(void* ptr)
{
    cudaError_t errorCode = cudaFree(ptr);

    if (errorCode != cudaSuccess) {
        fprintf(stderr, "cuda_free_checked: %s\n", cudaGetErrorString(errorCode));
        exit(-1);
    }
}

static inline void cuda_memcpy_to_device(void* dst, const void* src, size_t count)
{
    cudaError_t errorCode = cudaMemcpy(dst, src, count, cudaMemcpyHostToDevice);

    if (errorCode != cudaSuccess) {
        fprintf(stderr, "cuda_memcpy_to_device: %s\n", cudaGetErrorString(errorCode));
        exit(-1);
    }
}

static inline void cuda_memcpy_from_device(void* dst, const void* src, size_t count)
{
    cudaError_t errorCode = cudaMemcpy(dst, src, count, cudaMemcpyDeviceToHost);

    if (errorCode != cudaSuccess) {
        fprintf(stderr, "cuda_memcpy_from_device: %s\n", cudaGetErrorString(errorCode));
        exit(-1);
    }
}

static inline void cuda_memset_zero_checked(void* dst, size_t count)
{
    cudaError_t errorCode = cudaMemset(dst, 0, count);

    if (errorCode != cudaSuccess) {
        fprintf(stderr, "cuda_memset_zero_checked: %s\n", cudaGetErrorString(errorCode));
        exit(-1);
    }
}

/*
 * This function will allocate memory on GPU
 * and will copy the compat graph data structure
 */
void cuda_prepare_csr(const csrdata* csrg, csrdata* csrg_dev)
{
    UL nv = csrg->nv;
    UL ne = csrg->ne;

    cuda_malloc_checked( (void**) &csrg_dev->offsets, (nv+1) * sizeof(UL));
    cuda_malloc_checked( (void**) &csrg_dev->rows, ne * sizeof(UL)); 
    //cuda_malloc_checked( (void**) &csrg_dev->deg, nv * sizeof(UL));

    cuda_memcpy_to_device(csrg_dev->offsets, csrg->offsets, (nv+1) * sizeof(UL));
    cuda_memcpy_to_device(csrg_dev->rows, csrg->rows, ne * sizeof(UL));
    //cuda_memcpy_to_device(csrg_dev->deg, csrg->deg, nv * sizeof(UL));

    csrg_dev->nv = nv;
    csrg_dev->ne = ne;
}

void cuda_prepare_data(const struct kernel_data* src, struct kernel_data* dest)
{
    dest->nv = src->nv;
    
    cuda_malloc_checked( (void**) &dest->has_work, 1);
    cuda_malloc_checked( (void**) &dest->q1, src->nv);
    cuda_malloc_checked( (void**) &dest->q2, src->nv);
    cuda_malloc_checked( (void**) &dest->dist, src->nv*sizeof(UL));

    cuda_memcpy_to_device(dest->q1, src->q1, src->nv);
    cuda_memcpy_to_device(dest->dist, src->dist, src->nv*sizeof(UL));

    cuda_memset_zero_checked(dest->q2, src->nv);
}

void cuda_dist_from_dev(struct kernel_data* dev, struct kernel_data* host)
{
    cuda_memcpy_from_device(host->dist, dev->dist, host->nv*sizeof(UL));
}

void cuda_kernel_has_work(const struct kernel_data* dev, char* has_work)
{
    cuda_memcpy_from_device(has_work, dev->has_work, sizeof(char));
}

void cuda_free_data(struct kernel_data* kd_dev)
{
    cuda_free_checked(kd_dev->has_work);
    cuda_free_checked(kd_dev->q1);
    cuda_free_checked(kd_dev->q2);
    cuda_free_checked(kd_dev->dist);
}

void cuda_free_csr(const csrdata* csrg_dev)
{
     //cuda_free_checked(csrg_dev->deg);
     cuda_free_checked(csrg_dev->rows);
     cuda_free_checked(csrg_dev->offsets);
}

/* Dequeue and neighbors scan to setup a new frontier */
__global__ void cuda_neighbor(csrdata csrg, struct kernel_data data)
{
    const int WARPS_PER_BLOCK = blockDim.x/WARP_SIZE;

    int i, j, warpId;
    UL V, s, e;
    UL* neigh;

    *(data.has_work) = 0;

    warpId = blockIdx.x*WARPS_PER_BLOCK + threadIdx.x/WARP_SIZE;

    i = warpId;

    // printf("block=%d thread=%d belong to warpId %d\n", blockIdx.x, threadIdx.x, warpId);
    
    while (i < csrg.nv) {
        if (data.q1[i]) {

            data.q1[i] = 0;

            s = csrg.offsets[i];
            e = csrg.offsets[i+1] - s;

            neigh = &csrg.rows[s];

            for (j = threadIdx.x%WARP_SIZE; j < e; j += WARP_SIZE) {
                V = neigh[j];
                data.q2[V] = 1;
                // printf("b=%d w=%d t=%d element %d j=%d neigh_len=%d\n", blockIdx.x, warpId, threadIdx.x, i, threadIdx.x%32, e-s);
            }
        }

        i += (gridDim.x*blockDim.x)/WARP_SIZE;
    }
}

__global__ void cuda_next_nodes(struct kernel_data data)
{
    int i;
    UL prevDist;

    i = (blockIdx.x*blockDim.x)+threadIdx.x;

    while (i < data.nv) {
        if (data.q2[i] == 1) {
            *(data.has_work) = 1;
            data.q2[i] = 0;
            prevDist = data.dist[i];
            data.dist[i] = min(data.d+1,prevDist);
            data.q1[i] = (prevDist == UINT32_MAX);
        }
        i += gridDim.x*blockDim.x;
    }
}

