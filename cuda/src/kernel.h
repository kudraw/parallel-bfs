
#ifndef _KERNEL_H
#define _KERNEL_H

void cuda_prepare_csr(const csrdata* csrg, csrdata* csrg_dev);
void cuda_free_csr(const csrdata* csrg_dev);

void cuda_prepare_data(const struct kernel_data* src, struct kernel_data* dest);
void cuda_free_data(struct kernel_data* dest);

void cuda_kernel_has_work(const struct kernel_data* kd_dev, char* has_work);
void cuda_dist_from_dev(struct kernel_data* dev, struct kernel_data* host);

__global__ void cuda_next_nodes(struct kernel_data data);
__global__ void cuda_neighbor(csrdata csrg, struct kernel_data dev);

#endif
