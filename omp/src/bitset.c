
#include "bitset.h"

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <assert.h>

struct bitset bitset_init(size_t size)
{
    struct bitset b;

    uint32_t bytes = (size/8);

    uint32_t i;

    b._memory = calloc(1, bytes);

    if (!b._memory) {
        fprintf(stderr, "bitset_init: cannot allocate memory.\n");
        exit(EXIT_FAILURE);
    }

    uint32_t slots = bytes/8;

    fprintf(stderr, "Bitset Initialized, required %lu bits, allocated %u bytes [%u slots]\n", size, bytes, slots);

    for (i=0; i < slots; i++) {
        assert(b._memory[i] == 0);
    }

    return b;
}

void bitset_set(struct bitset* b, uint32_t index)
{
    uint64_t slot = (index/64);
    uint64_t bit = (index%64);

    b->_memory[slot] |= (1ul << bit);
}

uint32_t bitset_get(struct bitset* b, uint32_t index)
{
    uint64_t slot = (index/64);
    uint64_t bit = (index%64); 

    uint64_t value = (b->_memory[slot] & (1ul << bit));

    // fprintf(stderr, "bitset_get for index: %u => retreived: %lu\n", index, value);

    return !!value;
}
