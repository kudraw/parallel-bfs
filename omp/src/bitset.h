
#ifndef __BITSET_H
#define __BITSET_H

#include <stdint.h>
#include <stdlib.h>

struct bitset
{
    uint64_t* _memory;
};

struct bitset bitset_init(size_t bits);

void bitset_set(struct bitset* b, uint32_t index);

uint32_t bitset_get(struct bitset* b, uint32_t index);

#endif
