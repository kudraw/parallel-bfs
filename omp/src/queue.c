
#include "queue.h"

#include <stdio.h>

void queue_init(struct queue* q, size_t elements)
{
    q->_memory = malloc(elements * sizeof(uint32_t));

    if (!q->_memory) {
        fprintf(stderr, "queue_init: unable to allocate memory.\n");
        exit(EXIT_FAILURE);
    }

    printf("queue_init: allocated space @ %p for %lu elements (%lu bytes).\n", q->_memory, elements, elements*sizeof(uint32_t));

    q->_count = 0;
}

void queue_push(struct queue* q, uint32_t elem)
{
    q->_memory[q->_count++] = elem;
}

void queue_reset(struct queue* q)
{
    q->_count = 0;
}

void queue_free(struct queue* q)
{
    free(q->_memory);
}

void queue_dump(struct queue* q)
{
    const uint32_t size = q->_count;

    printf("queue @ %p => [", q->_memory);

    for (uint32_t i = 0; i < size; i++) {
        printf(" %u", q->_memory[i]);
    }

    printf(" ]\n");
}
