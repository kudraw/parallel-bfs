
#ifndef __QUEUE_H
#define __QUEUE_H

#include <stdint.h>
#include <stdlib.h>

struct queue
{
    uint32_t* _memory;
    uint32_t _count;
};

void queue_init(struct queue* q, size_t size);

void queue_push(struct queue* q, uint32_t elem);

uint32_t queue_get(const struct queue* q, uint32_t index);

void queue_reset(struct queue* q);

void queue_free(struct queue* q);

void queue_dump(struct queue* q);

#endif
