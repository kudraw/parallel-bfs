#!/usr/bin/python

import subprocess
import argparse
import statistics

EXEC_PATH="../bfs_omp"

NRUN=2
SCALE=4
DEGREE=4
GEN_METHOD=1
GRAPH_PATH=""

parser = argparse.ArgumentParser()
parser.add_argument('-path',help='Executable path')
parser.add_argument('-S',help="Scale exponent", type=int)
parser.add_argument('-N',help="Number of runs", type=int)
parser.add_argument('-E',help="Average degree", type=int)
parser.add_argument('-g',help="Average degree", type=int)

args = parser.parse_args()
if args.path:
    EXEC_PATH = args.path
if args.N:
    NRUN = args.N
if args.S:
    SCALE = args.S
if args.E:
    DEGREE = args.E
if args.g:
    if args.g == 0:
        print("Option -g 0 not allowed")
    else:
        GEN_METHOD=args.g

mono  = [0.0]*NRUN
multi = [0.0]*NRUN
speed = [0.0]*NRUN

output=""
i = 0
numeric_out = 0.0
while i < NRUN:
    proc = subprocess.Popen([EXEC_PATH,"-S "+str(SCALE),"-E "+str(DEGREE),"-g "+str(GEN_METHOD),"-V 1"],stdout=subprocess.PIPE)
    output = proc.stdout.read()
    #Do average
    lines = output.decode("ascii").split("\n")
    if not lines[-3] == "Well done!":
        print(output)
        print("Error after "+str(i+1)+" runs")
        exit(-1)
    print("Well done run "+str(i+1))
    values = lines[-2].split(",")
    mono[i]=float(values[0])
    multi[i]=float(values[1])
    speed[i]=float(values[2])
    i+=1

out = open("results.csv","a")

line = str(SCALE)+","+str(DEGREE)+","+str(statistics.mean(mono))+","+str(statistics.variance(mono))+","+str(statistics.mean(multi))+","+str(statistics.variance(multi))+","+str(statistics.mean(speed))+","+str(statistics.variance(speed))+"\n"

out.write(line)

#print("Results after "+str(i)+" runs with scale="+str(SCALE)+" and degree="+str(DEGREE)+". Graph generated with method "+str(GEN_METHOD))
#print("\tMean =\t\t"+str(statistics.mean(results)))
#print("\tVariance =\t"+str(statistics.variance(results)))


