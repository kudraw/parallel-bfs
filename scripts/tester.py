#!/usr/bin/python

import subprocess
import argparse
import statistics

EXEC_PATH="../bfs_omp"

NRUN=2
MODE=1
DEGREE=20
SCALE = 16
GEN_METHOD=2

parser = argparse.ArgumentParser()
parser.add_argument('-path',help='Executable path')
parser.add_argument('-M',help="Test mode (Scale or Degree fixed)", type=int)
parser.add_argument('-N',help="Number of runs", type=int)

args = parser.parse_args()
if args.path:
    EXEC_PATH = args.path
if args.N:
    NRUN = args.N
if args.M:
    MODE = args.M

#SCALE
if MODE == 1:
    i = 12
    while i<25:
        proc = subprocess.Popen(["python3.5", "runner.py","-S "+str(i),"-E "+str(DEGREE),"-g "+str(GEN_METHOD),"-N "+str(NRUN)],stdout=subprocess.PIPE)
        output = proc.stdout.read()
        print("Done with S="+str(i))
        i = i+2
#DEGREE
elif MODE == 2:
    i = 20
    while i<101:
        proc = subprocess.Popen(["python3.5", "runner.py","-S "+str(SCALE),"-E "+str(i),"-g "+str(GEN_METHOD),"-N "+str(NRUN)],stdout=subprocess.PIPE)
        output = proc.stdout.read()
        print("Done with E="+str(i))
        i = i+10
